from random import randint

player_name = input("Hi! What is your name? ")



# guess 1

month_number = randint (1, 12)
year_number = randint (1975, 2010)

print("Guess 1:", player_name, ", were you born in", month_number, "/", year_number, "?")

response = input("Yes or No? ")

if response == "yes":
    print("GOT IT!")
    exit()
else:
    print("Let me try again...")

#guess 2

month_number = randint (1, 12)
year_number = randint (1975, 2010)

print("Guess 2:" " Were you born in", month_number, "/", year_number, "?")

response = input("Yes or No? ")

if response == "yes":
    print("YAY, I knew it!")
    exit()
else:
    print("Aww, I'll try again!")

#guess 3

month_number = randint (1, 12)
year_number = randint (1975, 2010)

print("Guess 3:" " Were you born in", month_number, "/", year_number, "?")

response = input("Yes or No? ")

if response == "yes":
    print("Got it!")
    exit()
else:
    print("Lemme try again!")

# guess 4

month_number = randint (1, 12)
year_number = randint (1975, 2010)

print("Guess 4:" " Were you born in", month_number, "/", year_number, "?")

response = input("Yes or No? ")

if response == "yes":
    print("Finally!")
    exit()
else:
    print("Lemme try one more time!")

# guess 5

month_number = randint (1, 12)
year_number = randint (1975, 2010)

print("Guess 5:" " Were you born in", month_number, "/", year_number, "?")

response = input("Yes or No? ")

if response == "yes":
    print("Finally got it!")
    exit()
else:
    print("I guess I just don't know. Bye.")
